#!/usr/bin/env python3
import re
import csv 
import random 
import sys 
import argparse

csv.register_dialect('learn_quiz',delimiter=',', quotechar='"', 
                          escapechar="\\", doublequote = False)

NEW_Q = "NewQuestion"
OPTION_TEXT = 'Option'



class State(object):
    NOTHING = 0
    QUESTION = 1
    OPTIONS = 2

def randomize_options(writer,data,enable_random):
    if enable_random:
        random.shuffle(data)

    for datum in data:
        writer.writerow(datum)
    return
    

def quiz_rand(fi,fo,config):
        
    csv_data = csv.reader(fi,'learn_quiz')
    writer = csv.writer(fo,'learn_quiz',lineterminator='\n',quoting=csv.QUOTE_MINIMAL)

    i = 0
    S = State.NOTHING
    options = []

    for line in csv_data:
        if len(line) == 0:
            continue

        if S == State.NOTHING:
            if line[0] == NEW_Q:
                S = State.QUESTION
            writer.writerow(line)
                
        elif S == State.QUESTION:
            if line[0] == OPTION_TEXT:
                S = State.OPTIONS
                options = [];
                options.append(line)
            else:
                writer.writerow(line)

        elif S == State.OPTIONS:
            if line[0] == OPTION_TEXT:
                options.append(line)
            else:
                randomize_options(writer,options,True)
                writer.writerow([])
                writer.writerow(line)

                if line[0] == NEW_Q:
                    S = State.QUESTION
                else:
                    # should not be here
                    S = State.NOTHING
    # last one edge case
    if S == State.OPTIONS:
         randomize_options(writer,options,True)

def main() :
    parser = argparse.ArgumentParser(description="Randomize options for multiple choice questions.")
    parser.add_argument("-i",nargs='?',  metavar='input_file')
    parser.add_argument("output",nargs='?')
    args = parser.parse_args()

    input_file = sys.stdin
    output_file = sys.stdout

    #print(args)

    if (args.i != None):
        input_file = open(args.i, 'r', newline="")
    if (args.output != None):
            output_file = open(args.output, 'w', newline="")

    quiz_rand(input_file, output_file, {})
    
    input_file.close()
    output_file.close()

if __name__ == "__main__" :

    main()

